#!/bin/bash
#
# Helper script to deploy all app / service using Docker images from the private registry.

ordered_services=( review catalogue shop-app )
workspace_dir="../workspace"

for repo_name in ${ordered_services[@]}; do
  pushd ${workspace_dir}/${repo_name}
    if [ -f ./scripts/deploy.sh ]; then
      echo "********** Deploying Docker image for: ${repo_name}"
      ./scripts/deploy.sh
    fi
  popd
done
