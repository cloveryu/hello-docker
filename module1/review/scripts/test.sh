#!/bin/sh
#
# Run tests for the service.

# Define commonly used JAVA_HOME variable
export JAVA_HOME=/usr/lib/jvm/java-8-oracle

mvn clean surefire-report:report
