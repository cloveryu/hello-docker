#!/bin/bash
#
# Deploy image as container from a Docker registry.

image_name=${1:-review}
image_tag=${2:-latest}
container_name=${3:-review}
container_port=${4:-8082}
current_dir=$(dirname $(echo $0))

set -e

${current_dir}/undeploy.sh ${container_name}

# Start the new container.
docker run -d \
    -p ${container_port}:${container_port} \
    --restart=always \
    --name ${container_name} \
    ${image_name}:${image_tag}

echo "Deployed ${image_name}:${image_tag}"
