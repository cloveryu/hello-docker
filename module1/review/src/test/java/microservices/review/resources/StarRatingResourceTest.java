package microservices.review.resources;

import microservices.review.core.Rating;
import microservices.review.core.RatingDao;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import javax.ws.rs.core.Response;

import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class StarRatingResourceTest {

    //TODO: Refactor - use mock provider for ratings
    final RatingDao ratingDao = new RatingDao("ratings.json");

    @Mock
    private java.util.stream.Stream<microservices.review.core.Rating> mockRatingStream;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
    }

    StarRatingResource starRatingResource = new StarRatingResource(ratingDao);

    @Test
    public void shouldFetchRatingsForGivenAlbum() throws Exception {
        String albumId = "5eb6d21a-d827-4c4d-8a77-51c92aaac748";
        String hostName = "localhost";

        Response ratings = starRatingResource.getRatings(albumId, hostName);

        assertThat(ratings.getStatus(), is(200));
    }
}