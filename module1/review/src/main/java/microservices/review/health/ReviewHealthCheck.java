package microservices.review.health;

import com.codahale.metrics.health.HealthCheck;

public class ReviewHealthCheck extends HealthCheck {

  @Override
  protected Result check() throws Exception {
    return Result.healthy();
  }
}
