package microservices.review.resources;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import microservices.review.core.ErrorResponse;
import microservices.review.core.Rating;
import microservices.review.core.RatingDao;
import microservices.review.hateoas.HateoasLink;
import microservices.review.hateoas.StarRatingDto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.google.common.collect.ImmutableList;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

@Path("/star-ratings")
@Api(value = "/star-ratings",
    description = "Star rating resource, list of star ratings for each album")
@Produces(MediaType.APPLICATION_JSON)
public class StarRatingResource {

  final static Logger logger = LoggerFactory.getLogger(StarRatingResource.class);
  final private RatingDao ratingDao;

  public StarRatingResource(RatingDao ratingDao) {
    this.ratingDao = ratingDao;
  }

  @GET
  @Timed(name = "getRatings")
  @ApiOperation(value = "Returns star ratings", notes = "Star ratings for albums",
      response = StarRatingDto.class)
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 404, message = "No rating found for the given album id"),
      @ApiResponse(code = 500, message = "Internal server error")})
  public Response getRatings(
          @ApiParam(value = "Star rating for album Id", required = false) @QueryParam("album") String albumId,
          @HeaderParam("host") String host) throws Exception {

    Stream<Rating> ratings = ratingDao.load().stream();
    // find rating with matching rating id
    Optional<String> albumIdOption = Optional.ofNullable(albumId);
    Optional<Rating> ratingOption =
        albumIdOption.flatMap(aid -> ratings.filter(item -> item.albumId.equals(aid)).findFirst());


    // rating for album id requested
    if (albumIdOption.isPresent()) {
      // rating for album found
      if (ratingOption.isPresent()) {
        // add hateoas links to response
        Rating rating = ratingOption.get();
        ImmutableList<HateoasLink> links =
            new ImmutableList.Builder<HateoasLink>()
                .add(
                    new HateoasLink("self", "http://" + host + "/star-ratings?album="
                        + rating.albumId)).build();
        return Response.ok(new StarRatingDto(rating, links)).build();
      }
      // rating for album not found
      else {
        return Response
            .status(Status.NOT_FOUND)
            .entity(
                new ErrorResponse(new RuntimeException(
                    "Unable to find star rating for rating with id: " + albumId))).build();
      }
    }

    // return the entire list of star ratings
    List<StarRatingDto> ratingList =
        ratings.map(
            rating -> {
              ImmutableList<HateoasLink> links =
                  new ImmutableList.Builder<HateoasLink>().add(
                      new HateoasLink("self", "http://" + host + "/star-ratings?album="
                          + rating.albumId)).build();
              return new StarRatingDto(rating, links);
            }).collect(Collectors.toList());
    return Response.ok(ratingList).build();
  }
}
