package microservices.review;

import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import java.util.EnumSet;

import javax.servlet.DispatcherType;

import microservices.review.core.RatingDao;
import microservices.review.health.ReviewHealthCheck;
import microservices.review.resources.StarRatingResource;
import microservices.util.ApiCorrelationFilter;
import microservices.util.ApiErrorMapper;

import org.eclipse.jetty.servlets.CrossOriginFilter;

import com.wordnik.swagger.config.ConfigFactory;
import com.wordnik.swagger.config.ScannerFactory;
import com.wordnik.swagger.config.SwaggerConfig;
import com.wordnik.swagger.jaxrs.config.DefaultJaxrsScanner;
import com.wordnik.swagger.jaxrs.listing.ApiDeclarationProvider;
import com.wordnik.swagger.jaxrs.listing.ResourceListingProvider;
import com.wordnik.swagger.jaxrs.reader.DefaultJaxrsApiReader;
import com.wordnik.swagger.jersey.listing.ApiListingResourceJSON;
import com.wordnik.swagger.reader.ClassReaders;

public class Application extends io.dropwizard.Application<Configuration> {

  public static void main(String[] args) throws Exception {
    new Application().run(args);
  }

  @Override
  public String getName() {
    return "review_service";
  }

  @Override
  public void initialize(Bootstrap<Configuration> bootstrap) {
    bootstrap.addBundle(new AssetsBundle("/assets", "/api", "index.html"));
  }

  @Override
  public void run(Configuration configuration, Environment environment) {
    final RatingDao ratingDao = new RatingDao("ratings.json");

    // review resources
    environment.jersey().register(new StarRatingResource(ratingDao));

    // register healthcheck which is accessible via the admin interface
    environment.healthChecks().register("template", new ReviewHealthCheck());

    // log API requests and set response correlation id
    environment.servlets().addFilter("Api-Request-Logger", new ApiCorrelationFilter())
        .addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
    // set CORS headers so the Swagger UI can access our resources
    environment.servlets().addFilter("CORS-Swagger-Api-Docs", new CrossOriginFilter())
        .addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
    // map API errors into Json responses
    environment.jersey().register(ApiErrorMapper.class);

    // Swagger /api-docs endpoint and class scanner
    environment.jersey().register(new ApiListingResourceJSON());
    environment.jersey().register(new ApiDeclarationProvider());
    environment.jersey().register(new ResourceListingProvider());
    ScannerFactory.setScanner(new DefaultJaxrsScanner());
    ClassReaders.setReader(new DefaultJaxrsApiReader());
    SwaggerConfig config = ConfigFactory.config();
    config.setApiVersion("1.0.0");
    config.setBasePath("/");
  }
}
