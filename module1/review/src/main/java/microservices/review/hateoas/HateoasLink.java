package microservices.review.hateoas;

import microservices.util.Require;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Link to another resource")
public class HateoasLink {

  @ApiModelProperty(required = true)
  public final String rel;
  @ApiModelProperty(required = true)
  public final String href;

  public HateoasLink(String rel,
                     String href) {
    Require.notEmpty(rel, "Hateos rel is missing");
    Require.notEmpty(href, "Hateos href is missing");
    this.rel = rel;
    this.href = href;
  }
}
