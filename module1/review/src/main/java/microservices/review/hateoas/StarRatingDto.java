package microservices.review.hateoas;

import java.util.List;

import microservices.review.core.Rating;

import com.google.common.collect.ImmutableList;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Star rating")
public class StarRatingDto {

  @ApiModelProperty(required = true)
  public final String albumId;

  @ApiModelProperty(required = true)
  public final Integer stars;

  @ApiModelProperty(required = true)
  public final List<HateoasLink> links;

  public StarRatingDto(Rating rating, ImmutableList<HateoasLink> links) {
    this.albumId = rating.albumId;
    this.stars = rating.stars;
    this.links = links;
  }
}
