package microservices.review.core;

import microservices.util.Require;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Rating {

  public final String albumId;
  public final Integer stars;

  @JsonCreator
  public Rating(@JsonProperty("albumId") String albumId, @JsonProperty("stars") Integer stars) {
    Require.notEmpty(albumId, "Rating album id is missing");
    Require.notNull(stars, "Rating stars is missing");
    this.albumId = albumId;
    this.stars = stars;
  }
}
