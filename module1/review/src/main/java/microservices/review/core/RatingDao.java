package microservices.review.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import microservices.util.JsonDeserialiser;

import com.codahale.metrics.annotation.Timed;

/**
 * Persist and load the list of ratings to and from a json file.
 */
public class RatingDao {

  final public String ratingFileLocation;

  public RatingDao(String ratingFileLocation) {
    this.ratingFileLocation = ratingFileLocation;
  }

  @Timed(name = "loadRatingDao")
  public List<Rating> load() throws Exception {
    FileInputStream fip = null;
    try {
      File file = new File(ratingFileLocation);
      fip = new FileInputStream(file);
      return JsonDeserialiser.deserialiseListOf(fip, Rating.class);
    } finally {
      try {
        if (fip != null) {
          fip.close();
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
