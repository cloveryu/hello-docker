package microservices.util;

public class Require {

  static public void notEmpty(String value, String errorMessage) {
    if (value == null || value.trim().equals("")) {
      throw new IllegalArgumentException(errorMessage);
    }
  }

  static public void notNull(Object value, String errorMessage) {
    if (value == null) {
      throw new IllegalArgumentException(errorMessage);
    }
  }
}
