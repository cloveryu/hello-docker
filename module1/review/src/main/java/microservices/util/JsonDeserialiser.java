package microservices.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonDeserialiser {

  final static Logger logger = LoggerFactory.getLogger(JsonDeserialiser.class);

  static public <T> List<T> deserialiseListOf(InputStream json, Class<? super T> target)
      throws Exception {
    try {
      ObjectMapper mapper = new ObjectMapper();
      mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
                       false);
      // convert JSON string to Map
      return mapper.readValue(
          json,
          mapper.getTypeFactory().constructCollectionType(List.class,
                                                          Class.forName(target.getName())));
    } catch (Throwable e) {
      logger.error(
          "CALL deserialiseListOf with target=" + target.getName(), e);
      throw new IOException(
          "Unable to deserialize the given json to a List<"
          + target.getName() + ">", e);
    }
  }
}
