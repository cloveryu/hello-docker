#!/bin/bash
#
# Helper script to run service locally via jar file (not in Docker).

export JAVA_HOME=/usr/lib/jvm/java-8-oracle

jar_file=`ls target/${image_name}-*.jar | grep -v sources | head -1`

${JAVA_HOME}/bin/java -jar ${jar_file} server config.yml
