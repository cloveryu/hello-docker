#!/bin/bash

echo "Down - Start..."

repos="`cat repos.txt`"
workspace_dir="../workspace"
image_whitelist="(shop|review|catalogue)"

# Remove local workspace
rm -rf ${workspace_dir}

# Post - Cleanup (Remove Docker containers and images)
docker ps -q | xargs -I {} docker kill {}
docker ps -a -q | xargs -I {} docker rm --force {}
docker images | grep -E "${image_whitelist}" | awk '{print $3}' | uniq | xargs -I {} docker rmi --force {}

echo "Down - Done."
