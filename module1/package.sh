#!/bin/bash
#
# Helper script to build all app / service Docker images.

repos="`cat repos.txt`"
workspace_dir="../workspace"

for repo_name in ${repos}; do
  pushd ${workspace_dir}/${repo_name}
    if [ -f ./scripts/package.sh ]; then
      echo "********** Building Docker image for: ${repo_name}"
      ./scripts/package.sh
    fi
  popd
done
