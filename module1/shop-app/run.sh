#!/bin/sh
#
# Helper script to run shop-app locally (not in Docker)

export JAVA_HOME=/usr/lib/jvm/java-8-oracle
$JAVA_HOME/bin/java -jar target/shop-0.0.1-SNAPSHOT.jar server config.yml

