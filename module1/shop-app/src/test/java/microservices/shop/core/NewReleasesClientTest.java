package microservices.shop.core;

import microservices.consul.ReviewServiceLookup;
import microservices.service.CatalogueService;
import microservices.service.ReviewService;
import microservices.shop.Configuration;
import microservices.shop.hateoas.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class NewReleasesClientTest {
    @Mock
    private Configuration mockConfig;
    @Mock
    private ReviewService mockReviewService;
    @Mock
    private CatalogueService mockCatalogueService;
    private NewReleasesClient newReleasesClient;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
        newReleasesClient = new NewReleasesClient(mockReviewService, mockCatalogueService);;
    }


    @Test
    public void shouldReturnAlbumsWithStarRatings() throws Exception {
        String correlationId = "123";

        List<AlbumSummary> albumsSummary = new ArrayList<>();
        String albumId = "album-id";
        AlbumSummary albumSummary = new AlbumSummary(albumId, "title", "artistName", new ArrayList<HateoasLink>());
        albumsSummary.add(albumSummary);
        NewReleases newReleases = new NewReleases(albumsSummary);
        Album album = new Album(albumId, "title", "artistName", "releaseDate", new ArrayList<Track>(), new ArrayList<HateoasLink>());
        when(mockCatalogueService.getNewReleases(mockConfig, correlationId)).thenReturn(newReleases);
        when(mockCatalogueService.getAlbum(correlationId, albumSummary)).thenReturn(album);
        when(mockReviewService.getStarRatings(correlationId,mockConfig, albumId)).thenReturn(new StarRating(albumId, 3, new ArrayList<HateoasLink>()));

        List < AlbumWithStarRating > albums = newReleasesClient.fetch(mockConfig, correlationId);

        assertTrue(albums.size() > 0);
    }
}
