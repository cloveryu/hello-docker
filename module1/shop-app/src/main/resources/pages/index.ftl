<#-- @ftlvariable name="" type="com.thoughtworks.tutorial.microservice.shop.views.HomepageView" -->
<!DOCTYPE html>
<html lang="en">
  <head>

<!--
TODO add star glyphs (css classes or images in loop)
 -->

    <!-- Basic Page Needs
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta charset="utf-8">
    <title>Musik Shop</title>
    <!-- Mobile Specific Metas
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- FONT
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="stylesheet" href="/assets/css/opensans_regular_macroman/stylesheet.css">
    <link rel="stylesheet" href="/assets/css/opensans_bold_macroman/stylesheet.css">

    <!-- CSS
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="stylesheet" href="/assets/css/normalize.css">
    <link rel="stylesheet" href="/assets/css/skeleton.css">
    <link rel="stylesheet" href="/assets/css/main.css">
  </head>
  <body>
    <!-- Primary Page Layout
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <div class="container">

      <header>
        <h1>Musik Shop</h1>
      </header>

      <h2>New Releases</h2>

      <!-- ALBUM START -->

      <#list albums as albumWithRating>
        <div class="album-overview row">

        <div class="one-third column album-artwork">
          <div class="album-bg">
            <div class="album-image-wrapper">
              <img src="/assets/album_artwork/${albumWithRating.album.id}.jpg">

            </div>
          </div/>
        </div>

        <div class="two-thirds column">

          <div class="album-info">
            <h3 class="album-title">${albumWithRating.album.title?html}</h3>
            <p><strong>Artists:</strong>${albumWithRating.album.artistName?html}</p>
            <p><strong>Release Date:</strong>${albumWithRating.album.releaseDate}</p>
            <p class="album-rating"><strong>Rating:</strong>
            <#if albumWithRating.starRating.stars != -1>
              <#list 0..<albumWithRating.starRating.stars as i>
               <img src="/assets/images/star.jpg" alt=" " />
             </#list>
            <#else>
              <div class="star-error">Ratings could not be fetched at the moment</div>
            </#if>
            &nbsp;</p>
            <p class="album-availability"><strong>Availability:</strong> ??? in stock</p>
          </div>

          <button class="show-track-listing button button-primary">Show track listing</button>

        </div>

        <table class="u-full-width album-track-listing">
          <thead>
            <tr>
              <th>Title</th>
              <th>Duration</th>
            </tr>
          </thead>
          <tbody>
          <#if albumWithRating.album.tracks?has_content>
            <!-- TRACK LOOP START -->
            <#list albumWithRating.album.tracks as track>
                <tr><td>${track.title?html}</td><td>${(track.duration/60)?int?html}:${(track.duration%60)?int?html}</td></tr>
            <!-- TRACK LOOP END -->
            </#list>
          <!-- IF NO TRACKS -->
          <#else>
            <tr><td>No listing available</td><td></td></tr>
          </#if>
          </tbody>
        </table>
      </div>
      </#list>

      <!-- END ALBUM -->

      </div>

      <script src="/assets/js/vendor/jquery-1.11.2.min.js"></script>
      <script src="/assets/js/main.js"></script>
      <!-- End Document
      –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    </body>
  </html>
