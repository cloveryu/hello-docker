<#-- @ftlvariable name="" type="com.thoughtworks.tutorial.microservice.shop.views.HomepageView" -->
<!DOCTYPE html>
<html lang="en">
  <head>

<!--
TODO add star glyphs (css classes or images in loop)
 -->

    <!-- Basic Page Needs
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta charset="utf-8">
    <title>Musik Shop</title>
    <!-- Mobile Specific Metas
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- FONT
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="stylesheet" href="/assets/css/opensans_regular_macroman/stylesheet.css">
    <link rel="stylesheet" href="/assets/css/opensans_bold_macroman/stylesheet.css">

    <!-- CSS
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="stylesheet" href="/assets/css/normalize.css">
    <link rel="stylesheet" href="/assets/css/skeleton.css">
    <link rel="stylesheet" href="/assets/css/main.css">
  </head>
  <body>
    <!-- Primary Page Layout
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <div class="container">

      <header>
        <h1>Musik Shop</h1>
      </header>

      <img src="/assets/images/strong-warning.png" align="middle" alt=" " />
      <h2 class="page-error">Apologies, the application is not available now :( <br> </h2>
      <h4 class="page-error">Please try again after few minutes </h4>

      <script src="/assets/js/vendor/jquery-1.11.2.min.js"></script>
      <script src="/assets/js/main.js"></script>
      <!-- End Document
      –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    </body>
  </html>
