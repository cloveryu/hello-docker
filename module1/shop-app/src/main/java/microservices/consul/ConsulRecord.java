package microservices.consul;

import microservices.util.Require;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ConsulRecord {

  public final String address;
  public final Integer servicePort;

  @JsonCreator
  public ConsulRecord(@JsonProperty("Address") String address,
      @JsonProperty("ServicePort") Integer servicePort) {
    Require.notEmpty(address, "Consul record address is missing");
    Require.notNull(servicePort, "Consul record servicePort is missing");
    this.address = address;
    this.servicePort = servicePort;
  }

  public String resourceUri(String resourceUri) {
    return "http://" + address + ":" + servicePort + resourceUri;
  }
}
