package microservices.consul;

import java.util.List;

import microservices.shop.Configuration;
import microservices.util.JsonDeserialiser;

import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReviewServiceLookup {

    static public String resourceUri(Configuration config, String resource)
            throws Exception {
        String delimiter = ":";
        String protocol = "http://";
        return protocol +config.getReviewServiceHost()+ delimiter +config.getReviewServicePort()+resource;
    }
}
