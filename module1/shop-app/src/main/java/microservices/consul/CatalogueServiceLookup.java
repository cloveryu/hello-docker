package microservices.consul;

import microservices.shop.Configuration;
import microservices.util.JsonDeserialiser;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class CatalogueServiceLookup {

    static public String resourceUri(Configuration config, String resource)
            throws Exception {
        String delimiter = ":";
        String protocol = "http://";
        return protocol +config.getCatalogueServiceHost()+delimiter+config.getCatalogueServicePort()+resource;
    }
}
