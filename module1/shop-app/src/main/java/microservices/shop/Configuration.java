package microservices.shop;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Configuration extends io.dropwizard.Configuration {

  @NotEmpty
  private String consul = "localhost:8500";

  @NotEmpty
  private String reviewServiceHost;

  @NotEmpty
  private String reviewServicePort;

  @NotEmpty
  private String catalogueServiceHost;

  @NotEmpty
  private String catalogueServicePort;

  @JsonProperty
  public String getConsul() {
    String consulIp = System.getenv("CONSUL_PORT_8500_TCP_ADDR");
    String consulPort = System.getenv("CONSUL_PORT_8500_TCP_PORT");
    if (consulIp != null && consulPort != null) {
      return "http://" + consulIp + ":" + consulPort;
    }
    // return config value
    return consul;
  }

  @JsonProperty
  public void setConsul(String consul) {
    this.consul = consul;
  }

  @JsonProperty
  public String getReviewServiceHost() {
    return reviewServiceHost;
  }

  @JsonProperty
  public void setReviewServiceHost(String reviewServiceHost) {
    this.reviewServiceHost = reviewServiceHost;
  }

  @JsonProperty
  public String getReviewServicePort() {
    return reviewServicePort;
  }

  @JsonProperty
  public void setReviewServicePort(String reviewServicePort) {
    this.reviewServicePort = reviewServicePort;
  }

  @JsonProperty
  public String getCatalogueServiceHost() {
    return catalogueServiceHost;
  }

  @JsonProperty
  public void setCatalogueServiceHost(String catalogueServiceHost) {
    this.catalogueServiceHost = catalogueServiceHost;
  }

  @JsonProperty
  public String getCatalogueServicePort() {
    return catalogueServicePort;
  }

  @JsonProperty
  public void setCatalogueServicePort(String catalogueServicePort) {
    this.catalogueServicePort = catalogueServicePort;
  }
}
