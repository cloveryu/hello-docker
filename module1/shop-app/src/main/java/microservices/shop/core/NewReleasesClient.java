package microservices.shop.core;

import com.codahale.metrics.annotation.Timed;
import microservices.consul.CatalogueServiceLookup;
import microservices.consul.ReviewServiceLookup;
import microservices.service.CatalogueService;
import microservices.service.ReviewService;
import microservices.shop.Configuration;
import microservices.shop.hateoas.*;
import microservices.util.JsonDeserialiser;
import microservices.util.ShortUUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;

public class NewReleasesClient {

    final static Logger logger = LoggerFactory.getLogger(NewReleasesClient.class);
    private final ReviewService reviewService;
    private final CatalogueService catalogueService;

    public NewReleasesClient(ReviewService reviewService, CatalogueService catalogueService) {
        this.reviewService = reviewService;
        this.catalogueService = catalogueService;
    }

    @Timed(name = "fetchNewReleasesWithStarRating")
    public List<AlbumWithStarRating> fetch(Configuration config, String correlationId)
            throws Exception {

        NewReleases newReleases = catalogueService.getNewReleases(config, correlationId);

        Stream<AlbumWithStarRating> albums =
                newReleases.albums.stream().map(albumSummary -> {
                    try {
                        Album album = catalogueService.getAlbum(correlationId, albumSummary);
                        StarRating starRating = reviewService.getStarRatings(correlationId, config, albumSummary.getId());

                        return new AlbumWithStarRating(album, starRating);

                    } catch (Exception e) {
                        logger.error("Unable to fetch new release albums with star ratings, correlationId="
                                + correlationId, e);
                        throw new RuntimeException(e);
                    }
                });

        return albums.collect(Collectors.toList());
    }
}
