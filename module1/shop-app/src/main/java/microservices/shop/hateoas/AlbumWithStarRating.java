package microservices.shop.hateoas;

import microservices.util.Require;

public class AlbumWithStarRating {

    final public Album album;
    final public StarRating starRating;

    public AlbumWithStarRating(Album album, StarRating starRating) {
        Require.notNull(album, "Album missing");
        Require.notNull(starRating, "Star rating is missing");
        this.album = album;
        this.starRating = starRating;
    }

    public Album getAlbum() {
        return album;
    }

    public StarRating getStarRating() {
        return starRating;
    }
}
