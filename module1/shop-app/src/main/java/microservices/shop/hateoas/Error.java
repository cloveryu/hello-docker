package microservices.shop.hateoas;

import microservices.util.Require;

public class Error {

    final public String errorMessage;

    public Error(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
