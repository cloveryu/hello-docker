package microservices.shop.hateoas;

import microservices.util.Require;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class HateoasLink {

    public final String rel;
    public final String href;

    @JsonCreator
    public HateoasLink(
            @JsonProperty("rel") String rel,
            @JsonProperty("href") String href) {
        Require.notEmpty(rel, "Hateos rel is missing");
        Require.notEmpty(href, "Hateos href is missing");
        this.rel = rel;
        this.href = href;
    }

    public String getRel() {
        return rel;
    }

    public String getHref() {
        return href;
    }
}
