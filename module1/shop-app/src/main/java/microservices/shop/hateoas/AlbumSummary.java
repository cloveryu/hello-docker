package microservices.shop.hateoas;

import java.util.List;

import microservices.util.Require;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AlbumSummary {

    public final String id;
    public final String title;
    public final String artistName;
    public final List<HateoasLink> links;

    @JsonCreator
    public AlbumSummary(
            @JsonProperty("id") String id,
            @JsonProperty("title") String title,
            @JsonProperty("artistName") String artistName,
            @JsonProperty("links") List<HateoasLink> links) {
        Require.notEmpty(id, "Album id is missing");
        Require.notEmpty(title, "Album title is missing");
        Require.notEmpty(artistName, "Album artist name is missing");
        Require.notNull(links, "Album links are missing");
        this.id = id;
        this.title = title;
        this.artistName = artistName;
        this.links = links;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getArtistName() {
        return artistName;
    }

    public List<HateoasLink> getLinks() {
        return links;
    }
}
