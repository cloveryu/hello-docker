package microservices.shop.hateoas;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import microservices.util.Require;

public class NewReleases {

  public final List<AlbumSummary> albums;

  @JsonCreator
  public NewReleases(
          @JsonProperty("albums") List<AlbumSummary> albums) {
    Require.notNull(albums, "New releases albums are missing");
    this.albums = albums;
  }

  public List<AlbumSummary> getAlbums() {
    return albums;
  }
}
