package microservices.shop.hateoas;

import microservices.util.Require;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Track {

  public final String id;
  public final String title;
  public final Integer duration;

  @JsonCreator
  public Track(@JsonProperty("id") String id, @JsonProperty("title") String title,
               @JsonProperty("duration") Integer duration) {
    Require.notEmpty(id, "Track id is missing");
    Require.notEmpty(title, "Track title name is missing");
    Require.notNull(duration, "Track duration is missing");
    this.id = id;
    this.title = title;
    this.duration = duration;
  }

  public String getId() {
    return id;
  }

  public String getTitle() {
    return title;
  }

  public Integer getDuration() {
    return duration;
  }
}
