package microservices.shop.hateoas;

import java.util.List;

import microservices.util.Require;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class StarRating {

  public final String albumId;
  public final Integer stars;
  public final List<HateoasLink> links;

  @JsonCreator
  public StarRating(@JsonProperty("albumId") String albumId, @JsonProperty("stars") Integer stars,
                    @JsonProperty("links") List<HateoasLink> links) {
    Require.notEmpty(albumId, "Star rating album id is missing");
    Require.notNull(stars, "Star rating stars are missing");
    Require.notNull(links, "Star rating links are missing");
    this.albumId = albumId;
    this.stars = stars;
    this.links = links;
  }

  public String getAlbumId() {
    return albumId;
  }

  public Integer getStars() {
    return stars;
  }

  public List<HateoasLink> getLinks() {
    return links;
  }
}
