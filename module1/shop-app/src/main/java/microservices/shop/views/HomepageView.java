package microservices.shop.views;

import com.google.common.base.Charsets;
import io.dropwizard.views.View;
import microservices.shop.hateoas.AlbumWithStarRating;

import java.util.List;

public class HomepageView extends View {

  final private List<AlbumWithStarRating> albums;

  public HomepageView(List<AlbumWithStarRating> albums) {
    super("/pages/index.ftl", Charsets.UTF_8);
    this.albums = albums;
  }

  public List<AlbumWithStarRating> getAlbums() {
    return albums;
  }
}
