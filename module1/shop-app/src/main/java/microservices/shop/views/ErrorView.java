package microservices.shop.views;

import com.google.common.base.Charsets;
import io.dropwizard.views.View;
import microservices.shop.hateoas.AlbumWithStarRating;

import java.nio.charset.Charset;
import java.util.List;

public class ErrorView extends View {

  final private Error error;

  public ErrorView(Error error) {
    super("/pages/error.ftl", Charsets.UTF_8);
    this.error = error;
  }

  public Error getError() {
    return error;
  }
}
