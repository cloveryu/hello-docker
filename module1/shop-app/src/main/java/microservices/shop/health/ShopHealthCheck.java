package microservices.shop.health;

import com.codahale.metrics.health.HealthCheck;

public class ShopHealthCheck extends HealthCheck {

    @Override
    protected Result check() throws Exception {
        return Result.healthy();
    }
}
