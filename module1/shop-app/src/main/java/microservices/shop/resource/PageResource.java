package microservices.shop.resource;

import io.dropwizard.views.View;
import microservices.service.CatalogueService;
import microservices.service.ReviewService;
import microservices.shop.Configuration;
import microservices.shop.core.NewReleasesClient;
import microservices.shop.views.ErrorView;
import microservices.shop.views.HomepageView;
import microservices.util.ShortUUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/")
public class PageResource {

  final static Logger logger = LoggerFactory.getLogger(PageResource.class);
  final private Configuration config;

  public PageResource(Configuration config) {
    this.config = config;
  }

  @GET
  @Produces("text/html;charset=UTF-8")
  @Path("/")
  public View homepage() throws Throwable {
    String correlationId = ShortUUID.random();
    try {
      logger.debug("Rendering homepage, correlationId: " + correlationId);
      return new HomepageView(new NewReleasesClient(new ReviewService(), new CatalogueService()).fetch(config, correlationId));
    } catch (Throwable e) {
      logger.error("Error fetching new releases, correlationId: " + correlationId, e);
      return new ErrorView(new Error("Error fetching new releases"));
    }
  }
}
