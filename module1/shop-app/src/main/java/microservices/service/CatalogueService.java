package microservices.service;

import microservices.consul.CatalogueServiceLookup;
import microservices.shop.Configuration;
import microservices.shop.hateoas.Album;
import microservices.shop.hateoas.AlbumSummary;
import microservices.shop.hateoas.NewReleases;

public class CatalogueService {
    public NewReleases getNewReleases(Configuration config, String correlationId) throws Exception {
        return HttpService.getResource(
                CatalogueServiceLookup.resourceUri(config, "/new-releases"),
                correlationId,
                NewReleases.class);
    }

    public Album getAlbum(String correlationId, AlbumSummary albumSummary) throws Exception {
        return HttpService.getResource(
                albumSummary.links.stream()
                        .filter(link -> link.rel.equals("album"))
                        .findFirst().get().href,
                correlationId,
                Album.class);
    }
}
