package microservices.service;

import microservices.consul.ReviewServiceLookup;
import microservices.shop.Configuration;
import microservices.shop.hateoas.AlbumSummary;
import microservices.shop.hateoas.StarRating;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class ReviewService {

    final static Logger logger = LoggerFactory.getLogger(ReviewService.class);

    public StarRating getStarRatings(String correlationId, Configuration config, String albumId) throws Exception {
        // follow the albums' links in new releases to get albums' resources
        String starRatingEnpoint = ReviewServiceLookup.resourceUri(config, "/star-ratings");

        try {
            return HttpService.getResource(
                    starRatingEnpoint + "?album=" + albumId,
                    correlationId,
                    StarRating.class);
        } catch (Exception e) {
            logger.error("Exception while fetching the star ratings for " + albumId, e);
            return new StarRating(albumId, -1, new ArrayList<>());
        }
    }
}
