package microservices.service;

import com.codahale.metrics.annotation.Timed;
import microservices.util.JsonDeserialiser;
import microservices.util.ShortUUID;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class HttpService {

    final static Logger logger = LoggerFactory.getLogger(HttpService.class);

    @Timed(name = "getResource")
    static public <T> T getResource(final String resourceUri, String correlationId, Class<T> target)
            throws IllegalStateException, Exception {
        logger.debug("Getting resource: " + resourceUri + " [correlationId: " + correlationId + "]");
        HttpResponse response = getHttpResponse(resourceUri, correlationId);

        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode != 200) {
            throw new RuntimeException("Unable to fetch resource (status "
                    + statusCode + "): " + resourceUri
                    + " [correlationId: " + correlationId + "]");
        }

        logger.debug("Got response from '"
                + resourceUri + "': " + statusCode
                + " [correlationId: " + correlationId + "]");
        return JsonDeserialiser.deserialise(response.getEntity().getContent(), target);
    }

    public static HttpResponse getHttpResponse(String resourceUri, String childCorrelationId) throws IOException {
        return Request.Get(resourceUri)
                .addHeader("correlation-id", childCorrelationId)
                .connectTimeout(10000)
                .execute()
                .returnResponse();
    }

}
