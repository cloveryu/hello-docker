#!/bin/bash
#
# Builds a Docker image.

tag_name=${1:-latest}
image_name="shop-app"
jar_prefix="shop"
current_dir=$(dirname $(echo $0))
target_dir=${current_dir}/../target
docker_target_dir=${target_dir}/docker

set -e

${current_dir}/build.sh

jar_file=`ls target/${jar_prefix}-*.jar | grep -v sources | head -1`
image_files=( Dockerfile ${jar_file} config.yml )

# Copy files needed to build Docker image
mkdir -p ${docker_target_dir}
for file in "${image_files[@]}"; do
    echo "Copy file ${file} to ${docker_target_dir}"
    cp ${file} ${docker_target_dir}
done

# Build Docker image
pushd ${docker_target_dir}
   docker build -t ${image_name}:${tag_name} .
popd

${current_dir}/cleanup.sh
