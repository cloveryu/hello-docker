#!/bin/bash
#
# Remove unamed/tagged images:
# i.e. <none> <none>
#
# Run this to clean up after you successfully build or tag an image with same [repository]:[tag]

image_name="shop-app"

# Kill running containers that are based on the old image
docker ps | grep -E "\s${image_name}\s" | awk '{print $1}' | xargs -I {} docker kill {}

# Remove exited containers that are based on the old image
docker ps -a | grep -E "\s${image_name}\s" | awk '{print $1}' | xargs -I {} docker rm -f {}

# Remove dangling images
docker images -f 'dangling=true' -q | uniq | xargs -I {} docker rmi {}
