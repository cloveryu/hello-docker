#!/bin/bash
#
# Kills and remove all containers on the docker host matching service containers.

service_containers="(shop-app|review|catalogue)"

docker ps | grep -E "${service_containers}" | grep -v "CONTAINER ID" | awk '{print $1}' | xargs -I {} docker  kill {}
docker ps -a | grep -E "${service_containers}" | grep -v "CONTAINER ID" | awk '{print $1}' | xargs -I {} docker rm --force {}
