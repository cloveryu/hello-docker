#!/bin/bash

echo "UP - Start..."

repos="`cat repos.txt`"
workspace_dir="../workspace"
module_name=`basename $(pwd)`
state_name=`basename $(dirname $(pwd))`

./down.sh

# Clean up old workspace files
mkdir -p ${workspace_dir}
rm -rf ${workspace_dir}/*

# copy resources
for repo_name in ${repos}; do
  cp -a ${repo_name} ${workspace_dir}/
  find ${workspace_dir}/ -name "*.sh" -exec chmod 755 {} \;
done

cp -a references ${workspace_dir}/
cp *.yml ${workspace_dir}/

# Package Containers
./package.sh

# Deploy Containers
./deploy.sh

echo "UP - Done."
echo "---------------------------------------------------------------------------"
echo -e "Module [${module_name}] state [${state_name}] is ready in: ${workspace_dir}\n"
