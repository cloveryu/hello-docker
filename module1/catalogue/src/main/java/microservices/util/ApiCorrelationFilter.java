package microservices.util;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ApiCorrelationFilter implements javax.servlet.Filter {

  final static Logger logger = LoggerFactory.getLogger(ApiCorrelationFilter.class);

  @Override
  public void doFilter(ServletRequest request, ServletResponse response,
                       FilterChain chain) throws IOException, ServletException {
    HttpServletRequest req = (HttpServletRequest) request;
    HttpServletResponse res = (HttpServletResponse) response;
    // extract correlation-id and log request
    String correlationId = req.getHeader("correlation-id");
    logger.debug("API '" + requestedUrl(req) + "' requested, correlationId=" + correlationId);
    // add correlation-id the client has sent in the request to the response
    res.addHeader("correlation-id", correlationId);

    chain.doFilter(request, response);
  }

  private String requestedUrl(HttpServletRequest request) {
    return request.getScheme()
           + "://"
           + request.getServerName()
           + ("http".equals(request.getScheme()) && request.getServerPort() == 80
              || "https".equals(request.getScheme()) && request.getServerPort() == 443 ? "" : ":"
                                                                                              + request
        .getServerPort())
           + request.getRequestURI()
           + (request.getQueryString() != null ? "?" + request.getQueryString() : "");
  }

  @Override
  public void destroy() {
  }

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {
  }
}