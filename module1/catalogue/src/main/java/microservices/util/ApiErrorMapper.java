package microservices.util;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

import microservices.catalogue.core.ErrorResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ApiErrorMapper implements ExceptionMapper<Exception> {

  final static Logger logger = LoggerFactory.getLogger(ApiErrorMapper.class);

  @Context
  protected HttpServletRequest req;

  @Override
  public Response toResponse(Exception e) {
    String correlationId = req.getHeader("correlation-id");
    // log exceptions against the correlation id for tracing and debugging
    logger.error("Unexpected error thrown by API, correlationId=" + correlationId, e);
    if (e instanceof javax.ws.rs.NotFoundException) {
      return Response.status(Status.NOT_FOUND)
          .entity(new ErrorResponse(e))
          .build();
    }
    return Response.status(Status.INTERNAL_SERVER_ERROR)
        .entity(new ErrorResponse(e))
        .build();
  }
}