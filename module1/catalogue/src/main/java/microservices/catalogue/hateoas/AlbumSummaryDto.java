package microservices.catalogue.hateoas;

import java.util.List;

import microservices.catalogue.core.Album;

import com.google.common.collect.ImmutableList;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Album with abreviated details")
public class AlbumSummaryDto {

  @ApiModelProperty(required = true)
  public final String id;
  @ApiModelProperty(required = true)
  public final String title;
  @ApiModelProperty(required = true)
  public final String artistName;
  @ApiModelProperty(required = true)
  public final List<HateoasLink> links;

  public AlbumSummaryDto(Album album, ImmutableList<HateoasLink> links) {
    this.id = album.id;
    this.title = album.title;
    this.artistName = album.artistName;
    this.links = links;
  }
}
