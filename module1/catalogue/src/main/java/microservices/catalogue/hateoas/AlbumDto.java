package microservices.catalogue.hateoas;

import java.util.List;
import java.util.stream.Collectors;

import microservices.catalogue.core.Album;

import com.google.common.collect.ImmutableList;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Album with all details")
public class AlbumDto {

  @ApiModelProperty(required = true)
  public final String id;
  @ApiModelProperty(required = true)
  public final String title;
  @ApiModelProperty(required = true)
  public final String artistName;
  @ApiModelProperty(required = true)
  public final String releaseDate;
  @ApiModelProperty(required = true)
  public final List<TrackDto> tracks;
  @ApiModelProperty(required = true)
  public final List<HateoasLink> links;

  public AlbumDto(Album album, ImmutableList<HateoasLink> links) {
    this.id = album.id;
    this.title = album.title;
    this.artistName = album.artistName;
    this.releaseDate = album.releaseDate;
    this.tracks = album.tracks.stream()
        .map(track -> new TrackDto(track)).collect(Collectors.toList());
    this.links = links;
  }
}
