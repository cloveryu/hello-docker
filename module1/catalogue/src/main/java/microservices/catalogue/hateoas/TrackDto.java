package microservices.catalogue.hateoas;

import microservices.catalogue.core.Track;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Track details")
public class TrackDto {

  @ApiModelProperty(required = true)
  public final String id;
  @ApiModelProperty(required = true)
  public final String title;
  @ApiModelProperty(required = true)
  public final Integer duration;

  @JsonCreator
  public TrackDto(Track track) {
    this.id = track.id;
    this.title = track.title;
    this.duration = track.duration;
  }
}
