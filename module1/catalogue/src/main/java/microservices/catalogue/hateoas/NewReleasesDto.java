package microservices.catalogue.hateoas;

import java.util.List;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

@ApiModel(value = "List of newly released albums with abreviated details")
public class NewReleasesDto {

  @ApiModelProperty(required = true)
  public final List<AlbumSummaryDto> albums;

  public NewReleasesDto(List<AlbumSummaryDto> albums) {
    this.albums = albums;
  }
}
