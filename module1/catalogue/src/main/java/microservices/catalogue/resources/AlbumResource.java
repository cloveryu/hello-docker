package microservices.catalogue.resources;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import microservices.catalogue.core.Album;
import microservices.catalogue.core.AlbumDao;
import microservices.catalogue.core.ErrorResponse;
import microservices.catalogue.hateoas.AlbumDto;
import microservices.catalogue.hateoas.AlbumSummaryDto;
import microservices.catalogue.hateoas.HateoasLink;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.google.common.collect.ImmutableList;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

@Path("/albums")
@Api(value = "/albums", description = "Album resource, list of albums and detailed information about each album")
@Produces(MediaType.APPLICATION_JSON)
public class AlbumResource {

  final static Logger logger = LoggerFactory.getLogger(AlbumResource.class);
  final private AlbumDao albumDao;

  public AlbumResource(AlbumDao albumDao) {
    this.albumDao = albumDao;
  }

  @GET
  @Timed(name = "listAlbums")
  @ApiOperation(value = "Returns a list of all albums", notes = "Returns the list of albums with abreviated details"
      , response = AlbumSummaryDto.class, responseContainer = "List")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 500, message = "Internal server error")
  })
  public List<AlbumSummaryDto> listAlbums(@HeaderParam("host") String host) throws Exception {
    return albumDao.load().stream()
        .map(album -> {
          ImmutableList<HateoasLink> links = new ImmutableList.Builder<HateoasLink>()
              .add(new HateoasLink("self", "http://" + host + "/albums"))
              .add(new HateoasLink("album", "http://" + host + "/albums/" + album.id))
              .build();
          return new AlbumSummaryDto(album, links);
        })
        .collect(Collectors.toList());
  }

  @GET
  @Path("/{albumId}")
  @Timed(name = "getAlbum")
  @ApiOperation(value = "Find an album by ID", notes = "Returns album details"
      , response = AlbumDto.class)
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 404, message = "Album not found"),
      @ApiResponse(code = 500, message = "Internal server error")
  })
  public Response getAlbum(
      @ApiParam(value = "ID of album to fetch", required = true) @PathParam("albumId") String albumId,
      @HeaderParam("host") String host) throws Exception {
    // find album with matching album id
    Optional<Album> albumOption = albumDao.load()
        .stream()
        .filter(item -> item.id.equals(albumId))
        .findFirst();
    // album found
    if (albumOption.isPresent()) {
      // add hateoas links to response
      Album album = albumOption.get();
      ImmutableList<HateoasLink> links = new ImmutableList.Builder<HateoasLink>()
          .add(new HateoasLink("self", "http://" + host + "/albums/" + album.id))
          .add(new HateoasLink("list", "http://" + host + "/albums"))
          .build();
      return Response.ok(new AlbumDto(album, links)).build();
    }
    // album not found
    else {
      return Response.status(Status.NOT_FOUND)
          .entity(
              new ErrorResponse(new RuntimeException("Unable to find album with id: " + albumId)))
          .build();
    }
  }
}
