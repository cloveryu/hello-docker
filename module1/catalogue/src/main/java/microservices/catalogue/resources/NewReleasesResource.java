package microservices.catalogue.resources;

import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import microservices.catalogue.core.AlbumDao;
import microservices.catalogue.core.ErrorResponse;
import microservices.catalogue.hateoas.AlbumSummaryDto;
import microservices.catalogue.hateoas.HateoasLink;
import microservices.catalogue.hateoas.NewReleasesDto;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.google.common.collect.ImmutableList;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

@Path("/new-releases")
@Api(value = "/new-releases", description = "Newly released albums and links to the album details")
@Produces(MediaType.APPLICATION_JSON)
public class NewReleasesResource {

  final static Logger logger = LoggerFactory.getLogger(NewReleasesResource.class);
  final private AlbumDao albumDao;

  public NewReleasesResource(AlbumDao albumDao) {
    this.albumDao = albumDao;
  }

  @GET
  @Timed(name = "listNewReleases")
  @ApiOperation(value = "Returns a list of newly released albums"
      , notes = "Returns the list of albums with abreviated details"
      , response = NewReleasesDto.class)
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 400, message = "When an invalid limit has been given"),
      @ApiResponse(code = 500, message = "Internal server error")
  })
  public Response listNewReleases(
      @ApiParam(value = "Limit of new releases that should be returned") @QueryParam("limit") String limitParam
      , @HeaderParam("host") String host) throws Exception {
    if (limitParam != null && !StringUtils.isNumeric(limitParam))
      return Response.status(Status.BAD_REQUEST)
          .entity(new ErrorResponse(new RuntimeException("Limit must be a number")))
          .build();

    java.util.Optional<Long> limitOpt = java.util.Optional.ofNullable(
        limitParam == null ? null : Long.parseLong(limitParam));
    List<AlbumSummaryDto> albums = albumDao.load().stream()
        .limit(limitOpt.orElse(100l))
        .map(album -> {
          String limitString = limitOpt.map(limit -> "?limit=" + limit).orElse("");
          ImmutableList<HateoasLink> links = new ImmutableList.Builder<HateoasLink>()
              .add(new HateoasLink("self", "http://" + host + "/new-releases" + limitString))
              .add(new HateoasLink("album", "http://" + host + "/albums/" + album.id))
              .build();
          return new AlbumSummaryDto(album, links);
        })
        .collect(Collectors.toList());
    return Response.ok(new NewReleasesDto(albums))
        .build();
  }
}
