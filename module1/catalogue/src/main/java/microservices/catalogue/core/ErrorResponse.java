package microservices.catalogue.core;

public class ErrorResponse {

  public final String message;

  public ErrorResponse(Throwable e) {
    this.message = e.getMessage();
  }
}
