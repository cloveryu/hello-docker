package microservices.catalogue.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import microservices.catalogue.hateoas.AlbumDto;
import microservices.util.JsonDeserialiser;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Persist and load the list of albums to and from a json file.
 */
public class AlbumDao {

  final public String albumFileLocation;

  public AlbumDao(String albumFileLocation) {
    this.albumFileLocation = albumFileLocation;
  }

  public void persist(List<AlbumDto> albums)
      throws JsonGenerationException, JsonMappingException, IOException {
    FileOutputStream fop = null;
    try {
      File file = new File(albumFileLocation);
      fop = new FileOutputStream(file);

      if (!file.exists()) {
        file.createNewFile();
      }

      ObjectMapper mapper = new ObjectMapper();
      mapper.writerWithDefaultPrettyPrinter().writeValue(fop, albums);

      fop.flush();
      fop.close();
    } finally {
      try {
        if (fop != null) {
          fop.close();
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  @Timed(name = "loadAlbumsDao")
  public List<Album> load() throws Exception {
    FileInputStream fip = null;
    try {
      File file = new File(albumFileLocation);
      fip = new FileInputStream(file);
      return JsonDeserialiser.deserialiseListOf(fip, Album.class);
    } finally {
      try {
        if (fip != null) {
          fip.close();
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
