package microservices.catalogue.core;

import java.util.List;

import microservices.util.Require;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Album {

  public final String id;
  public final String title;
  public final String artistName;
  public final String releaseDate;
  public final List<Track> tracks;

  @JsonCreator
  public Album(@JsonProperty("id") String id,
               @JsonProperty("title") String title,
               @JsonProperty("artistName") String artistName,
               @JsonProperty("releaseDate") String releaseDate,
               @JsonProperty("tracks") List<Track> tracks) {
    Require.notEmpty(id, "Album id is missing");
    Require.notEmpty(title, "Album title is missing");
    Require.notEmpty(artistName, "Album artist name is missing");
    Require.notNull(releaseDate, "Album release date is missing");
    Require.notNull(tracks, "Album tracks are missing");
    this.id = id;
    this.title = title;
    this.artistName = artistName;
    this.releaseDate = releaseDate;
    this.tracks = tracks;
  }
}
