#!/bin/sh
#
# Publish a Docker image to a Docker registry.

image_id=${1:-catalogue}
image_tag=${2:-latest}
image_name=${3:-catalogue}
registry_server="registry:5000"
current_dir=$(dirname $(echo $0))

set -e

# Tags local image "awesome:latest"  as "registry:5000/awesome:latest"
# so that it can be pushed to a private Docker registry.
docker tag -f ${image_id}:${image_tag} ${registry_server}/${image_name}:${image_tag}
docker push ${registry_server}/${image_name}:${image_tag}

${current_dir}/cleanup.sh
