#!/bin/bash
#
# Undeploy previously deployed image as container.
# - Stops running container
# - Removes stopped container

container_name=${1:-catalogue}

set -e

# Stop and remove old container.
docker ps | grep -E "${container_name}" | awk '{print $1}' | xargs -I {} docker kill {}
docker ps -a | grep -E "${container_name}" | awk '{print $1}' | xargs -I {} docker rm -v {}

echo "Undeployed ${container_name}"
