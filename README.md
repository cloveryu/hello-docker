## Hello Docker

### Prepare
1. 确保环境中Java和Maven可用
2. 启动Docker Machine
3. 运行module1目录下的`./up.sh`

**浏览器中打开{machine ip}:8080**

****

### Will learn
* Volume management
* Handling application configuration

#### How to deal with data in Docker?

Data in container？ What's the problem?

#### Volume
* Normal directories on the host file system
* I/O performance on volume is exactly same as I/O on host
* Volume content is not included in the docker image
* Any changes to volume content (via RUN) is not part of image
* Can be shared and reused among containers
* Persists even if the container itself is deleted

##### Initialize Volume
* Volumes mounted from host
* Volumes mounted from other containers

##### Volumes mounted from host
allows any directory/file to be shared between host OS and container

`docker run -v /os/config.yml:/config.yml`

*Exercise - Mount volume from host filesystem*

* Create the directory to be volumed
* Run a container and mount volume at runtime
* Check if the directory exists in the container
* add some files in volume, check host
* remove the container and check host

##### Volumes mounted from other containers
Share directory between containers

`docker run --volume-from some-container`

*Exercise - Mount volume from container*

* look at the shop-app Dockerfile
* Does the current shop-app container have a copy of that config file?
* Create data-only container (references, Dockerfile, build image, check history)
* Use data-only container from shop-app (stop all services, update shop-app Dockerfile, re-package, update docker-compose, create data container, deploy all service - compose, check)

#### Configuration Management
Options to manage environments specific configuration
* Bake configuration into containers
* Use Environment variables
* Lookup in key-value store
* Mount volumes from host
* Mount volumes from other containers

##### Bake configuration into containers
* Using 'COPY' directive from Dockerfile
* Using 'RUN' directive to modify them during image build time

Easiest option if the configuration is static

##### Use Environment variables
* Pass environment variables when starting a container

  `docker run -e USERNAME=zzz PASSWORD=...`

Works fine for a simple configuration

##### Lookup in key-value store
* Using key-value store for getting config values (consul, etcd, zookeeper)
* Makes the configuration more dynamic, Introduces an external dependency, availability becomes critical
